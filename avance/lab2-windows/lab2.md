# Lab 2
## Objectif 

Configurer des groupes de serveurs Windows à l’aide d’un playbook
Créer le fichier lab_2.yaml  dans le répertoire playbooks. 



Objectifs : 
- Explorer le fichier group_vars/windows.yaml qui contient les informations de connexion au serveur Windows.
- Installer Firefox
- Activer la fonctionnalité IIS
- Ouvrer le port 80 du firewall windows
- Copier un fichier index.html dans C:\inetpub\wwwroot\
- Ajouter l’usager local bob avec le mot de passe redhat
- Commetter votre playbook  
